//
//  GraphViewModel.swift
//  RibsHellth
//
//  Created by Hardik's Mac on 22/11/21.
//

import Foundation
import RxSwift
import Charts
import RxCocoa


// MARK: Created seprete view model to prepare data and display on screen
class GraphViewModel:NSObject
{
    static let sharedInstance = GraphDataManager()
    var WeekDays = ["MON","TUE","WED","THU","FRI","SAT","SUN"]
    var days = ["08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00"]
    var Months = (0..<30).map { (i) -> String in
        return String(format: "%02d", i)
    }
    var Staps = 1
    // MARK: Fetch All Data in Model
    class func fetchGraphData(type:SegmentType) -> Observable<[ChartModel]>
    {
        
        return Observable<[ChartModel]>.create { observer  in
            var Chart = [ChartModel]()
            switch (type)
            {
            case .day :
                Chart = [
                    ChartModel(mLineData: LineViewController(Lineentries: GraphDataManager.generateLineData(selectedType: .day)), mBarData: BarViewSetup(Barentries:  GraphDataManager.generateBarData(selectedType: .day)), mPieData: PiechartViewSetup(entries:  GraphDataManager.generatePieData()), YAxis: sharedInstance.days,steps: DataManager.Staps)
                ]
            case .week :
                Chart = [
                    ChartModel(mLineData: LineViewController(Lineentries: GraphDataManager.generateLineData(selectedType: .week)), mBarData: BarViewSetup(Barentries:  GraphDataManager.generateBarData(selectedType: .week)), mPieData: PiechartViewSetup(entries:  GraphDataManager.generatePieData()), YAxis: sharedInstance.WeekDays,steps: DataManager.Staps)
                    ]
            case .months :
                Chart = [
                    ChartModel(mLineData: LineViewController(Lineentries: GraphDataManager.generateLineData(selectedType: .months)), mBarData: BarViewSetup(Barentries:  GraphDataManager.generateBarData(selectedType: .months)), mPieData: PiechartViewSetup(entries:  GraphDataManager.generatePieData()), YAxis: sharedInstance.Months,steps: DataManager.Staps)
                    ]
            }
            
            observer.onNext(Chart)
            return Disposables.create()
        }
    }
    // MARK: Setup Linedataset View
    class func LineViewController(Lineentries:[ChartDataEntry]) -> LineChartData
    {
        
        let set = LineChartDataSet(entries: Lineentries, label: "Runtime")
        set.setColor(UIColor(red: 247/255, green: 159/255, blue: 51/255, alpha: 1))
        set.lineWidth = 2.5
        set.setCircleColor(UIColor.red)
        set.circleRadius = 5
        set.circleHoleRadius = 2.5
        set.fillColor = UIColor(red: 240/255, green: 238/255, blue: 70/255, alpha: 1)
        set.mode = .cubicBezier
        set.drawValuesEnabled = true
        set.valueFont = .systemFont(ofSize: 10)
        set.valueTextColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
        set.mode = .linear
        set.axisDependency = .left
        return LineChartData(dataSet: set)
    }
    // MARK: Setup Bardataset View
    class func BarViewSetup(Barentries:[ChartDataEntry]) -> BarChartData
    {
        let set1 = BarChartDataSet(entries: Barentries, label: "Steps")
        set1.setColor(UIColor(red:99/255, green: 162/255, blue: 251/255, alpha: 1))
        set1.valueTextColor = .clear
        set1.valueFont = .systemFont(ofSize: 10)
        set1.axisDependency = .left
        let groupSpace = 0.06
        let barSpace = 0.02 // x2 dataset
        let barWidth = 0.5 // x2 dataset
        // (0.45 + 0.02) * 2 + 0.06 = 1.00 -> interval per "group"
        var dataSets = [BarChartDataSet]()
        dataSets.append(set1)
        let data = BarChartData(dataSets: dataSets)
        data.barWidth = barWidth
        // make this BarData object grouped
        data.groupBars(fromX: 0, groupSpace: groupSpace, barSpace: barSpace)
        return BarChartData(dataSet: set1)
    }
    // MARK: Setup Piechartdataset View
    class func PiechartViewSetup(entries:[ChartDataEntry]) -> PieChartData
    {
        let set = PieChartDataSet(entries: entries, label: "")
        set.drawIconsEnabled = false
        set.sliceSpace = 2
        
        set.colors = [UIColor(red: 247/255, green: 159/255, blue: 51/255, alpha: 1),UIColor(red: 99/255, green: 162/255, blue: 251/255, alpha: 1),UIColor(red: 104/255, green: 145/255, blue: 199/255, alpha: 1)]
        
        let data = PieChartData(dataSet: set)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1
        pFormatter.percentSymbol = " %"
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        data.setDrawValues(false)
        data.setValueFont(.systemFont(ofSize: 11, weight: .light))
        data.setValueTextColor(.black)
        return data
    }
}
