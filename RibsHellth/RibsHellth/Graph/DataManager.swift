//
//  DataManager.swift
//  RibsHellth
//
//  Created by Hardik's Mac on 22/11/21.
//

import Foundation
import RxSwift
import Charts
import RxCocoa

enum SegmentType
{
    case day
    case months
    case week
}
let DataManager = GraphDataManager.sharedInstance
class GraphDataManager:NSObject
{
    static let sharedInstance = GraphDataManager()
    var WeekDays = ["MON","TUE","WED","THU","FRI","SAT","SUN"]
    var days = ["08:00","09:00","10:00","11:00","12:00","13:00","14:00","15:00"]
    var Months = (0..<30).map { (i) -> String in
        return String(format: "%02d", i)
    }
    var Staps = 1
    
 // MARK: Create Linechart dummy data
    class func generateLineData(selectedType: SegmentType) -> [ChartDataEntry] {
        var Count = Int()
        var StartValue =  Int()
        var StopValue =  Int()
        DataManager.Staps = 0
        switch selectedType
        {
        case .day:
            Count = GraphDataManager().days.count
            StartValue = 100
            StopValue = 300
            break
        case .months:
            Count = GraphDataManager().Months.count
            StartValue = 100
            StopValue = 500
            break
        case .week:
            Count = GraphDataManager().WeekDays.count
            StartValue = 100
            StopValue = 500
            break
        }
        let Lineentries = (0..<Count).map { (i) -> ChartDataEntry in
            return BarChartDataEntry(x: Double(i), y: Double(Int.random(in: StartValue...StopValue)))
        }
        return Lineentries
    }

 // MARK: Create Barchart dummy data
    class func generateBarData(selectedType:SegmentType) -> [ChartDataEntry] {
        var Count = Int()
        var StartValue =  Int()
        var StopValue =  Int()
        DataManager.Staps = 0
        switch selectedType
        {
        case .day:
            Count = GraphDataManager().days.count
            StartValue = 100
            StopValue = 1000
            break
        case .months:
            Count = GraphDataManager().Months.count
            StartValue = 500
            StopValue = 1000
            break
        case .week:
            Count = GraphDataManager().WeekDays.count
            StartValue = 300
            StopValue = 2000
            break
        }
        let  Barentries = (0..<Count).map { (i) -> ChartDataEntry in
            let YAxis = Double.random(in: Double(StartValue)...Double(StopValue))
            DataManager.Staps = DataManager.Staps + Int(YAxis)
            return BarChartDataEntry(x: Double(i), y: YAxis)
            
        }
       
        return Barentries
    }
    
// MARK: Create Piechart dummy data
    class func generatePieData() -> [ChartDataEntry] {
        let PieChartName =  ["Jog","Run","Climb"]
        let entries = (0..<PieChartName.count).map { (i) -> ChartDataEntry in
            return PieChartDataEntry(value: Double(arc4random_uniform(5) + 10 / 5), label: PieChartName[i])
        }
        return entries
    }
}
